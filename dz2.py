print("Введите количество программистов в команде:")
programmers_count = None
while programmers_count is None:
    try:
        programmers_count = int(input())
        if programmers_count < 1:
            programmers_count = None
            print("Необходимо ввести число больше 0")
    except ValueError:
        print("Введите количество программистов в команде в числовом целом формате")

result = []

print("Теперь введите число оценки каждого программиста:")
for i in range(programmers_count):
    print(f"Оценка программиста №{i+1}: ", end='')
    score = None
    while score is None:
        try:
            score = float(input())
            if score <= 0:
                score = None
                print("Необходимо ввести число больше 0")
                print(f"Оценка программиста №{i + 1}: ", end='')
            else:
                result.append(score)
        except ValueError:
            print("Введите оценку в числовом формате")

max_score = max(result)
min_score = min(result)
margin_score = max_score - min_score

print(f"Максимальная оценка: {max_score}; Минимальная оценка: {min_score}; Разница между оценками: {margin_score}")
print(result)

with open("ya_rabotau.txt", "a") as f:
    for score in result:
        f.write(str(score) + " ")

