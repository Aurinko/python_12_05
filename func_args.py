def sum_numbers(a: float, b: float, c=2.0):
    print(f"a={a} b={b} c={c}")
    return a + b + c


result = sum_numbers(1, 2, 10 + 10)
print("Result:", result)
result = sum_numbers(1, 2)
print("Result:", result)
result = sum_numbers(a=3, b=5, c=10)
print("Result:", result)
result = sum_numbers(b=4, c=123, a=3)
print("Result:", result)

mean_temperature_june = sum_numbers(32, 33, 35) / 3
mean_temperature_january = -25
mean_temperature_february = -16
result = sum_numbers(a=mean_temperature_june,
                     b=mean_temperature_february,
                     c=mean_temperature_january)
print(result)

# text = input("Enter text: ")
# print(text, type(text))