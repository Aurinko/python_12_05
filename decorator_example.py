from time import time

# start = time()


def my_decorator(func):
    def wrapper(*args, **kwargs):
        start = time()

        print("Start")
        func(*args, **kwargs)
        print("Finish")

        finish = time()
        print("Time of function:", finish - start)
    return wrapper


@my_decorator
def sum1(x):
    print(f"{x} + 1 = {x+1}")


@my_decorator
def sum2(x):
    print(f"{x} + 2 = {x+2}")


sum1(10)
sum2(20)

# Для пустой траты времени
# Не имеет отношения к декораторам и нужен для демонстрации time
# for i in range(100000):
#     j = 0
#
# finish = time()
# print("TIME:", finish - start)