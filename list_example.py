if __name__ == "__main__":
    numbers = [111, 2, 5, 40]

    # for item in numbers:
    #     print("Элемент", item)

    numbers.append(60)
    numbers.append(60)
    # print(numbers)

    numbers.insert(2, "AAAAAAAAAAAAAA")
    # print(numbers)

    tmp = numbers.pop(2)
    # print(tmp, numbers)
    # print(len(tmp))

    numbers.sort(reverse=True)
    # print(numbers)

    # objects_list = [1, 3.5, "Jjjjjjjj", False, 0]
    # print(objects_list)

    numbers2 = numbers.copy()
    # print(numbers, numbers2)
    numbers2.append("100500")
    # print(len(numbers), len(numbers2))

    numbers = list(range(10, 100, 3))
    print(numbers)
    print(numbers[::3])

    status_list = ["Добавлено", "Обработано", "Отклонено"]
    # first = status_list[0]
    first = status_list.pop(0)
    print(status_list)

    print(len(numbers))

    numbers = [1, 1, 1, 3, 4, 3, 2, 1]
    print(set(numbers))
