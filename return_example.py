def abs(number):
    if number >= 0:
        return number
    else:
        return -number
#
#
# print(abs(-10))
# print(abs(100))


def abs_str(text: str):
    number = int(text)
    abs_value = abs(number)
    return number, abs_value

#
# result = abs_str("-1243")
# print(result)
# n, a = result
# print(n, a)
#
# n, a = abs_str("-1243")
