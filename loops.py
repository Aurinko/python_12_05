if __name__ == "__main__":
    names = ["Ann", "Kate", "John", "Jack", 123]

    for i, name in enumerate(names):
        print(f"Имя пользователя №{i + 1}: {name}")

    ages = [20, 25, 19, 5, 123]

    for age, name in zip(ages, names):
        print(f"Возраст {name} {age} лет")

    cities = ["Абакан", "Москва", "Иркутск"]
    for age, name, city in zip(ages, names, cities):
        print(f"Возраст {name} {age} лет. Родной город: {city}")
        if city == "Иркутск":
            print("Привет, иркутяне")

    print("Пример для while")
    #
    # text = ""
    # while text != "стоп":
    #     text = input("Введите текст: ")
    #     print(f"Введен текст: {text}")

    while True:
        text = input("Введите текст: ")
        print(f"Введен текст: {text}")
        if text == "стоп":
            print("Стоп...")
            break
        if text == "далее":
            continue

        print("Какая-то обработка\n\n")

