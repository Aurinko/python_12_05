def my_caps_loop_print(*values):
    for value in values:
        print(value.upper(), end=" ")  # Для верхнего регистра
        # print(value.lower())  # Для нижнего регистра
    print()


def my_caps_print(*values):
    caps_values = [value.upper() for value in values]
    print(*caps_values)


my_caps_print("a", "Ann", "Abakan")
print("a", "Ann", "Abakan")