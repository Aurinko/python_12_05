print("NAME: ", __name__)


def calc_rectangle_area(a, b):
    return a * b


def calc_circle_area(r):
    return 3.1415926535 * r ** 2
