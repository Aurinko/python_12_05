from return_example import abs_str
from math import pi
import utils.example as ex

print(ex.calc_rectangle_area(10, 3))
print(ex.calc_circle_area(2))


def calc_circle_area(d):
    r = d / 2
    return pi * r ** 2


if __name__ == "__main__":
    print(calc_circle_area(4))

    a = "-100"
    a = abs_str(a)
    print(a)