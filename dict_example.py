words_dict = {
    "car": "машина",
    "apple": ["яблоко"],
    0: "ноль",
    ("key", 1, 3): ["ключ", "один", "три"]
}

# print(words_dict)
# print(words_dict["apple"])
print(words_dict.keys())

for k in words_dict.keys():
    print(f"Ключ: {k}. Значение: {words_dict[k]}")

value_list = words_dict[("key", 1, 3)].copy()
print(value_list, type(value_list))
value_list.append("AAAAAAA")
print(value_list)

for k in words_dict.keys():
    print(f"Ключ: {k}. Значение: {words_dict[k]}")

words_dict["laptop"] = "ноутбук"
print(words_dict)

words_dict["apple"].append("Компания Эпл")
print(words_dict)

database = {
    "150-098-97908": 9,
    "Петров": 2,
    "Семенов": 6,
    "Иванов": 10
}

print(database)