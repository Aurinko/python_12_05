class Student:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self):
        pass


class StupidStudent(Student):
    def __init__(self, name, age):
        super().__init__(name, age)
        self.debt_counter = 0

    def say_hello(self):
        print(f"Всем превет! Миня завут {self.name}")

    def fail(self):
        self.debt_counter += 1
        print(f"Сессия провалена. Теперь у меня {self.debt_counter} долгов")


class GeniusStudent(Student):
    def __init__(self, name, age):
        super().__init__(name, age)

    def say_hello(self):
        print(f"Приветствую Вас! Тятенька нарек меня {self.name} {self.age} лет назад!")


class MiddleStudent(Student):
    def __init__(self, name, age):
        super().__init__(name, age)

    def say_hello(self):
        print(f"Я середнячок! Меня зовут {self.name}")


s = StupidStudent("Иван", 17)
s.say_hello()

g = GeniusStudent("Иваний", 17)
g.say_hello()

m = MiddleStudent("Ваня", 18)
m.say_hello()
print(m.name)
m.name = 0
print(m.name)
m.say_hello()
m.age = -1000
