def calc_mean_age(students_list):
    age_sum = 0
    for student in students_list:
        age_sum += student.__age
    return age_sum / len(students_list)


class Student:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self):
        print(f"Всем привет! Меня зовут {self.name}")


class StupidStudent(Student):
    def __init__(self, name, age):
        super().__init__(name, age)
        self.debt_counter = 0

    def fail(self):
        self.debt_counter += 1
        print(f"Сессия провалена. Теперь у меня {self.debt_counter} долгов")


s = StupidStudent("Ivan", 20)
s.say_hello()
for i in range(6):
    s.fail()
