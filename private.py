class Student:
    def __init__(self, name, age):
        self.__name = name
        self.__age = age

    def say_hello(self):
        print(f"Меня зовут {self.__name}. Мне {self.__age} лет")

    def print_name(self):
        print(self.__name)


student1 = Student("Kate", 20)
student1.say_hello()
# print(student1.name)
student1.print_name()
