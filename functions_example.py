def print_hi(name):
    name = name.upper()
    print(f"Hi! {name}!")


# user = input("Enter name: ")
user = "Kate"
print_hi(user)
print(user)


def print_list(seq):
    seq.append("AAA")
    for i in seq:
        print(i)

# Как изменился список
lst = [1, 2, 4, "g"]
print(lst, len(lst))
print_list(lst)
print(lst, len(lst))

# Копия списка
lst1 = lst.copy()
lst2 = lst
lst1.append("ОДИН!!!")
lst2.append("ДВА!!!")
print(lst)

print("Проверка с копией списка:")
print(lst, len(lst))
print_list(lst.copy())
print(lst, len(lst))
